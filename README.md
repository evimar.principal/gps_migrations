**Plot one year of GPS data of 3 migration birds.**

Thanks to Cartopy for allow us to project the migration trajectories into a map.

Graphs:  

- [Direct Plot of GPS trajectories](https://gitlab.com/evimar.principal/gps_migrations/blob/master/direct_trajectories.pdf)
- [GPS Trajectories using a cartographic projection](https://gitlab.com/evimar.principal/gps_migrations/blob/master/cartographic_trajectories.pdf)
- [Speed Distribution of each bird](https://gitlab.com/evimar.principal/gps_migrations/blob/master/Speed%20Distributions.pdf)

You can find the code here: [gps_plot.py](https://gitlab.com/evimar.principal/gps_migrations/blob/master/gps_plot.py)